(function ($) {
	"use strict";

    jQuery(document).ready(function($){


        $(".embed-responsive iframe").addClass("embed-responsive-item");
        $(".carousel-inner .item:first-child").addClass("active");
        
        $('[data-toggle="tooltip"]').tooltip();


		$('.mainmenu').slicknav();

        $('.homepage-slides').owlCarousel({
            items:1,
            loop:true,          
            dots:false,
            nav:true,
            autoplay:true,
            navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right">']
   
        
        
        });

     $('.portfolio-menu li').click(function(){ 
        
      $('.portfolio-menu li').removeClass("active");
      $(this).addClass("active");        

        var selector = $(this).attr('data-filter'); 
        $('.portfolio-items').isotope({ 
            filter: selector, 
            animationOptions: { 
                duration: 750, 
                easing: 'linear', 
                queue: false, 
            } 
        }); 
      return false; 
    });
	   //counter
     $('.counter').counterUp({
        delay: 10,
        time: 1000
    });

        


    });


    jQuery(window).load(function(){

                // Active isotope in container
   
    $('.portfolio-items').isotope({
        itemSelector: '.portfolio-item',
        layoutMode: 'fitRows',
        
    });

        
    });


}(jQuery));	